# Plugin

This is a basic wordpress plugin. It adds a custom gutenberg block which
renders the current temperature of Dresden or Cottbus (it's your choice!).

# Installing

1) Pull into wordpress plugin dir.

2) `npm i`

3) `npm run dev`

4) Activate plugin.

5) Add block. (Widget)

