<?php
/**
 * Plugin Name: 3m5 Gutenberg Block - Open Weather Map
 * Plugin URI: -
 * Description: Gutenberg block showing current temperature of Dresden or Cottbus (OpenWeatherMap API).
 * Author: Felix Hellwig
 * Author URI: 3m5.de
 * Version: 1.0.0
 * License: GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

defined( 'ABSPATH' ) || exit;


/**
 * Enqueue the block's assets for the editor.
 *
 * wp-blocks:  The registerBlockType() function to register blocks.
 * wp-element: The wp.element.createElement() function to create elements.
 * wp-i18n:    The __() function for internationalization.
 *
 * @since 1.0.0
 */
function _3m5_gb_omw_script_enqueue() {
	wp_enqueue_script(
		'3m5-gb-omw-script',
		plugins_url( 'js/block.build.js', __FILE__ ),
		array( 'wp-blocks', 'wp-i18n', 'wp-element' )
	);
	register_block_type( 'threem5/open-weather-map', array(
		'render_callback' => 'render_current_temperature',
	) );
}

add_action( 'init', '_3m5_gb_omw_script_enqueue' );

/**
 * Renders the current temperature on the actual page where the block is used.
 *
 * @return String html code including p-tag and temperature from json from owm api.
 *
 * @since 1.0.0
 */
function render_current_temperature( $attributes ) {
	$apiurl = 'https://api.openweathermap.org/data/2.5/weather?q=' . $attributes['city'] . ',de&units=metric&appid=2eee8def090fec7b57335b635d369e54';
	$data   = json_decode( file_get_contents( $apiurl ) );
	$temp   = $data->main->temp;

	return '<p>Current temperature: ' . $temp . '°C</p>';
}