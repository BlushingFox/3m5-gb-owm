import $ from "jquery";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'threem5/open-weather-map', {
	title: __( '3m5 Open Weather Map' ),
	icon: 'cloud',
    category: 'widgets',
    attributes: {
        city: {
            type: 'string',
            default: 'Dresden'
        }
    },
    /*
     * edit() describes what u see when u use the block in the editor.
     *
     * Here u have a select where u can choose the city u want to have the temperature from.
     * When changing the value the attribute will be set (setCity()), 'Dresden' is default.
     */
    edit ( props ) {
        const { attributes: { city }, setAttributes } = props;

        function setCity( event ) {
            const selected = event.target.querySelector( 'option:checked' );
            setAttributes( { city: selected.value } );
            event.preventDefault();
        }

        return (
            <form onSubmit={ setCity }>
            <p>Choose your city:</p>
                <select value={ city } onChange={ setCity }>
                    <option value="dresden">Dresden</option>
                    <option value="cottbus">Cottbus</option>
                </select>
            </form>
        );
    },
    /*
     * save() describes what u see when u save and view the actual page.
     *
     * Here it returns null because we have a rendering function in 3m5-gb-own.php due to dynamic
     * content.
     */
    save ( props ) {
        return null;
    }
} );